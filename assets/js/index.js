// run when all assets on website is ready
$(window).on('load', function () {
	//check if ran on inapp browser
	var userAgent = window.navigator.userAgent.toLowerCase(),
		inapp = /instagram|fbav|fban|kakao|snapchat|line|micromessenger|twitter|wechat/.test(userAgent),
		ios = /iphone|ipod|ipad/.test(userAgent),
		safari = /safari/.test(userAgent),
		android = /android/.test(userAgent),
		chrome = /chrome/.test(userAgent),
		location = window.location.href;
	
	//clipboard actions
	var clipboard = new ClipboardJS('.btn');
	
	clipboard.on('success', function(e) {
		alert("Copied to clipboard!")
	});
	
	clipboard.on('error', function(e) {
		alert("Copy to clipboard error!")
	});
	
	//if inapp browser is detected then the user will be given instructions
	if (inapp) {
		$('.prompt-container').show();
		$('.content, a-scene').remove();
		$('#copy-input-link').val(location);
		if (android) {
			$(".browser-logo").attr("src","assets/img/chrome.png");
			$('p').each(function() {
				var text = $(this).text();
				text = text.replace('Safari', 'Chrome').replace('iOS', 'Android'); 
				$(this).text(text);
			});
		}
	} else {
		//if it's detected on ios, but not using safari, then we will open safari
		if (ios && !safari) {
			window.open('x-web-search://?ar', "_self");
			window.location.href = location;
		} else if (android && !chrome) {
			//if it's on android, but not using chrome then we will open chrome
			window.location.href = 'googlechrome://navigate?url=' + location;
		} else {
			//Fetch camera permission for AR/AFRAME
			var facingCamera = "user";
			if(ios || android){
				facingCamera = "environment";
			}

			var constraints = { 
				video: {
					advanced: [
						{
							facingMode: facingCamera
						}
					]
				} 
			};

			var errorCallback = function(error) {
				if (error.name == 'NotAllowedError') {
					alert("Please enable camera for AR experience.\r\nSetting > Browser name > Allow camera")
				}
			};
			navigator.mediaDevices.getUserMedia(constraints)
				.then(null, errorCallback);

			AFRAME.registerComponent("foo", {
				init: function () {
					//action on pinch and pan on the model to rotate and scale accordingly'
					var element = document.querySelector('body');
					var model = document.querySelector('#animated-model');
					var hammertime = new Hammer(element);
					var pinch = new Hammer.Pinch(); // Pinch is not by default in the recognisers
					hammertime.add(pinch); // add it to the Manager instance
			
					//scale => zoom and shrink model on pinch
					const minScale = 0.0025;
					const maxScale = 0.009; 
			
					hammertime.on("pinch", (ev) => {
						let pinch = ev.scale;
						let scale = model.getAttribute("scale").x*pinch;
						//limit zoom
						if(scale <= maxScale && scale >= minScale){
							let newScale = {x:scale, y:scale, z:scale}
							model.setAttribute("scale", newScale);
						}
					});
					
					model.addEventListener('model-loaded', e => {
						$(".arjs-loader").remove();
					})
			
					var key, animationRan = false, objectName;
					var buttonCurtain = $('.animation-btn.curtain');
					var buttonLamp = $('.animation-btn.lamp');
					var buttonAc = $('.animation-btn.Ac');
			
					var animations = 
						{
							curtain :  ["curtainAction.001", "curtain2Action"],
							lamp : ["Cone.001Action"],
							Ac: ["CurveAction","CurveAction.001","CurveAction.002","30Action", "29Action", "28Action","27Action", "26Action", "25Action","24Action", "23Action", "22Action","21Action", "20Action"]
						};
					
					var marker = document.querySelector('#animated-marker');
					var isMarkerFound = false;
					marker.addEventListener('markerFound', () => {
						isMarkerFound = true;
					});
			
					marker.addEventListener('markerLost', () => {
						isMarkerFound = false;
					});
			
					function playAnimationSequentially(){
						if(key < animations[objectName].length && isMarkerFound){
							model.setAttribute('animation-mixer', { clip: animations[objectName][key++], loop: 'repeat', duration: 1, crossFadeDuration: 0.1, timeScale: 1});
						} else {
							model.setAttribute('animation-mixer', {timeScale: 0});
							animationRan = false;
						}
					}
			
					buttonCurtain.click(function() {
						onClickAnimationButton("curtain");
					});
			
					buttonLamp.click(function() {
						onClickAnimationButton("lamp");
			
					});
					
					buttonAc.click(function() {
						onClickAnimationButton("Ac");
					});
			
					function onClickAnimationButton(animateObject){
						if(isMarkerFound){
							key = 0;
							if(animationRan){
								model.removeEventListener('animation-loop', playAnimationSequentially);
							} else {
								animationRan = true;
							}
							objectName = animateObject;
							model.setAttribute('animation-mixer', { clip: animations[animateObject][key++], loop: 'repeat', duration: 1, crossFadeDuration:0.1, timeScale: 1});
							model.addEventListener('animation-loop', playAnimationSequentially);
						}
					}
				}
			});
		}
	}
});
